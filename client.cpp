#include <iostream>
#include <memory>
#include <string>

#include <grpc++/grpc++.h>

#include "testproto.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;

using testproto::InputM;
using testproto::OutputM;
using testproto::TstProto;

class TstProtoClient {
 public:
        TstProtoClient(std::shared_ptr < Channel > channel)
        :stub_(TstProto::NewStub(channel)) {
        } int Perform(const int &user) {
                InputM request;
                request.set_istatus(user);

                OutputM reply;
                ClientContext context;
                Status status = stub_->Perform(&context, request, &reply);

                if (status.ok()) {
                        return reply.ostatus();
                } else {
                        std::cout << status.error_code() << ": " << status.error_message() << std::endl;
                        return -1;
                }
        }

 private:
        std::unique_ptr < TstProto::Stub > stub_;
};

int main(int argc, char **argv)
{
        TstProtoClient client(grpc::CreateChannel("localhost:50051", grpc::InsecureChannelCredentials()));
        int myval = 392;        // My univ roll (loll!)
        int reply = client.Perform(myval);
        std::cout << "TstProto received: " << reply << std::endl;
        return 0;
}
