#include <iostream>
#include <memory>
#include <string>

#include <grpc++/grpc++.h>

#include "testproto.grpc.pb.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

using testproto::InputM;
using testproto::OutputM;
using testproto::TstProto;

class TstProtoImpl final:public TstProto::Service {
        Status Perform(ServerContext * context, const InputM * inp, OutputM * outp) override {
                outp->set_ostatus(inp->istatus() + 2);
                return Status::OK;
}};

void RunServer()
{
        std::string server_address("0.0.0.0:50051");
        TstProtoImpl service;

        ServerBuilder builder;
        builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
        builder.RegisterService(&service);
        std::unique_ptr < Server > server(builder.BuildAndStart());
        std::cout << "Server listening on " << server_address << std::endl;

        server->Wait();
}

int main()
{

        RunServer();
        return 0;
}
